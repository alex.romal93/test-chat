import React from 'react';
import ReactDOM from 'react-dom';
import initStore from 'config/store';
import App from 'containers/App'
import registerServiceWorker from './registerServiceWorker';

const store = initStore();

ReactDOM.render(<App store={store} />, document.getElementById('root'));
registerServiceWorker();

if (module.hot && process.env.NODE_ENV !== 'production') {
  module.hot.accept();
}
