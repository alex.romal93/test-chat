export const wsUrl = 'wss://echo.websocket.org';

export const MESSAGE_TYPE_TEXT = 'MESSAGE_TYPE_TEXT';
export const MESSAGE_TYPE_IMAGE = 'MESSAGE_TYPE_IMAGE';
export const MESSAGE_TYPE_VIDEO = 'MESSAGE_TYPE_VIDEO';

export const SORT_DATE_ASC = 'SORT_DATE_ASC';
export const SORT_DATE_DESC = 'SORT_DATE_DESC';

export const FILTER_ALL = 'FILTER_ALL';
export const FILTER_TEXT = 'FILTER_TEXT';
export const FILTER_IMAGE = 'FILTER_IMAGE';
export const FILTER_VIDEO = 'FILTER_VIDEO';
