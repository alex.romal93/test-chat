import { combineReducers } from "redux";
import loginReducer from 'containers/Login/LoginReducer';
import chatReducer from 'containers/Chat/ChatReducer';

export default combineReducers({
  login: loginReducer,
  chat: chatReducer
});
