import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import rootReducer from "./reducers";

const initStore = (initialState = {}) => {
  const enhancers = applyMiddleware(logger);
  return createStore(rootReducer, initialState, enhancers);
};

export default initStore;
