// @flow

export type LoginState = {
  name: ?string
};

export type LoginSetNameAction = {
  type: "LOGIN:SET_NAME",
  name: string
};
export type LoginUnsetNameAction = {
  type: "LOGIN:CLOSE_SESSION"
};

export type LoginAction =
  | LoginSetNameAction
  | LoginUnsetNameAction;

export const LOGIN_SET_NAME = "LOGIN:SET_NAME";
export const LOGIN_UNSET_NAME = "LOGIN:UNSET_NAME";
