// @flow

import * as types from 'containers/Login/LoginTypes';
import type {
  LoginState,
  LoginAction
} from 'containers/Login/LoginTypes';

const initialState: LoginState = {
  name: null
};

const loginReducer = (
  state: LoginState = initialState,
  action: LoginAction
): LoginState => {
  switch (action.type) {
    case types.LOGIN_SET_NAME:
      return {
        ...state,
        name: action.name
      };
    case types.LOGIN_UNSET_NAME:
      return {
        ...state,
        name: null
      };
    default:
      return state;
  }
};

export default loginReducer;
