// @flow

import * as types from 'containers/Login/LoginTypes';
import type {
  LoginSetNameAction,
  LoginUnsetNameAction
} from 'containers/Login/LoginTypes';

export const loginSetName = (
  name: string
): LoginSetNameAction => ({
  type: types.LOGIN_SET_NAME,
  name
});

export const loginUnsetName = (
): LoginUnsetNameAction => ({
  type: types.LOGIN_UNSET_NAME
});
