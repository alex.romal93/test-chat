// @flow

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loginSetName } from 'containers/Login/LoginActions';
import LoginForm from 'components/LoginForm';

type Props = {
  +loginSetName: Function
};

class Login extends React.Component<Props> {
  render() {
    return (
      <LoginForm login={this.props.loginSetName} />
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ loginSetName }, dispatch);

export default connect(null, mapDispatchToProps)(Login);
