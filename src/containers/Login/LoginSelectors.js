// @flow

import type { LoginState } from 'containers/Login/LoginTypes';

type State = {
  login: LoginState
};

export const getLoginName = (state: State) => state.login.name;
