// @flow

import type { ChatState } from 'containers/Chat/ChatTypes';

type State = {
  chat: ChatState
};

export const getChatMessages = (state: State) => state.chat.messages;
export const getChatFilter = (state: State) => state.chat.filter;
export const getChatSort = (state: State) => state.chat.sort;
