// @flow

import * as types from 'containers/Chat/ChatTypes';
import type {
  ChatState,
  ChatAction
} from 'containers/Chat/ChatTypes';
import {
  FILTER_ALL,
  SORT_DATE_ASC
} from 'config/constants';

const initialState: ChatState = {
  messages: [],
  sort: SORT_DATE_ASC,
  filter: FILTER_ALL
};

const ChatReducer = (
  state: ChatState = initialState,
  action: ChatAction
): ChatState => {
  switch (action.type) {
    case types.CHAT_ADD_MESSAGE:
      return {
        ...state,
        messages: [...state.messages, action.message]
      };
    case types.CHAT_SET_SORT:
      return {
        ...state,
        sort: action.sort
      };
    case types.CHAT_SET_FILTER:
      return {
        ...state,
        filter: action.filter
      };
    default:
      return state;
  }
};

export default ChatReducer;
