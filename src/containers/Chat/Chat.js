// @flow

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import type { MessageType } from 'types/MessageType';
import { getLoginName } from 'containers/Login/LoginSelectors';
import { loginUnsetName } from 'containers/Login/LoginActions';
import {
  chatAddMessage,
  chatSetSort,
  chatSetFilter
} from 'containers/Chat/ChatActions';
import ChatWrapper from 'components/ChatWrapper';
import Login from 'containers/Login';
import Welcome from 'components/Welcome';
import { wsUrl } from 'config/constants';
import MessageForm from 'components/MessageForm';
import MessagesList from 'components/MessagesList';
import Filter from 'components/Filter';
import {
  MESSAGE_TYPE_IMAGE,
  MESSAGE_TYPE_TEXT,
  MESSAGE_TYPE_VIDEO
} from 'config/constants';
import {
  getChatMessages,
  getChatFilter,
  getChatSort
} from 'containers/Chat/ChatSelectors';

type Props = {
  +messages: Array<MessageType>,
  +loginName: ?string
};

type PropsFn = {
  +loginUnsetName: Function,
  +chatAddMessage: Function,
  +chatSetSort: Function,
  +chatSetFilter: Function
};

class Chat extends React.Component<Props & PropsFn> {
  socket: ?WebSocket = null;

  constructor(props) {
    super(props);
    this.sendMessage = this.sendMessage.bind(this)
  }

  static detectMessageType(message) {
    const extension = message.replace(/^\s*https?:\/\/.+\.(.+)\s*$/i, '$1');

    switch (extension) {
      case 'mp4':
        return MESSAGE_TYPE_VIDEO;
      case 'jpg':
      case 'jpeg':
        return MESSAGE_TYPE_IMAGE;
      default:
        return MESSAGE_TYPE_TEXT;
    }
  }

  onSocketOpen() {
  }

  onSocketClose() {
  }

  onSocketMessage(data) {
    try {
      const message = JSON.parse(data.data);
      this.props.chatAddMessage(message);
    } catch (e) {}
  }

  onSocketError() {
  }

  sendMessage(message) {
    const data: Message = {
      name: this.props.loginName,
      type: Chat.detectMessageType(message),
      data: message,
      date: Date.now()
    };

    this.socket.send(JSON.stringify(data));
  }

  componentDidMount() {
    this.socket = new WebSocket(wsUrl);
    this.socket.onopen = this.onSocketOpen;
    this.socket.onclose = this.onSocketClose;
    this.socket.onmessage = this.onSocketMessage.bind(this);
    this.socket.onerror = this.onSocketError;
  }

  componentWillUnmount() {
    if (this.socket) {
      this.socket.close();
    }
  }

  render() {
    const {
      loginName,
      messages,
      sort,
      filter,
      loginUnsetName,
      chatAddMessage,
      chatSetSort,
      chatSetFilter
    } = this.props;

    return (
      <div>
        {
          loginName
            ? <ChatWrapper>
                <Welcome name={loginName} logout={loginUnsetName} />
                <MessageForm key="message-form" send={this.sendMessage} />
                <Filter
                  sort={sort}
                  filter={filter}
                  setSort={chatSetSort}
                  setFilter={chatSetFilter}
                />
                {messages && <MessagesList messages={messages} sort={sort} filter={filter} />}
              </ChatWrapper>
            : <Login />
        }
      </div>
    );
  }
}

const mapStateToProps = (state): Props => ({
  loginName: getLoginName(state),
  sort: getChatSort(state),
  filter: getChatFilter(state),
  messages: getChatMessages(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({
      loginUnsetName,
      chatAddMessage,
      chatSetSort,
      chatSetFilter
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
