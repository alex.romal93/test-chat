// @flow

import type { MessageType } from 'types/MessageType';

export type ChatState = {
  messages: Array<MessageType>,
  sort: string,
  filter: string
};

export type ChatAddMessageAction = {
  type: "CHAT:ADD_MESSAGE",
  message: MessageType
};
export type ChatSetSortAction = {
  type: "CHAT:SET_SORT",
  sort: string
};
export type ChatSetFilterAction = {
  type: "CHAT:SET_FILTER",
  filter: string
}
export type ChatAction =
  | ChatAddMessageAction
  | ChatSetSortAction
  | ChatSetFilterAction;

export const CHAT_ADD_MESSAGE = "CHAT:ADD_MESSAGE";
export const CHAT_SET_SORT = "CHAT:SET_SORT";
export const CHAT_SET_FILTER = "CHAT:SET_FILTER";
