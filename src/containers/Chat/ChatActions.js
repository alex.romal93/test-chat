// @flow

import * as types from 'containers/Chat/ChatTypes';
import type { MessageType } from 'types/MessageType';
import type {
  ChatAddMessageAction,
  ChatSetFilterAction,
  ChatSetSortAction
} from 'containers/Chat/ChatTypes';

export const chatAddMessage = (
  message: MessageType
): ChatAddMessageAction => ({
  type: types.CHAT_ADD_MESSAGE,
  message
});

export const chatSetSort = (
  sort: string
): ChatSetSortAction => ({
  type: types.CHAT_SET_SORT,
  sort
});

export const chatSetFilter = (
  filter: string
): ChatSetFilterAction => ({
  type: types.CHAT_SET_FILTER,
  filter
});
