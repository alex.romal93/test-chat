// @flow

import React from 'react';
import { Provider } from 'react-redux';
import type { Store } from 'redux';
import Chat from 'containers/Chat';

type Props = {
  +store: Store
};

class App extends React.Component<Props> {
  render() {
    const { store } = this.props;

    return (
      <Provider store={store}>
        <Chat />
      </Provider>
    );
  }
}

export default App;
