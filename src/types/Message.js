// @flow

export type MessageType = {
  name: string,
  type: string,
  data: string,
  date: Date
};
