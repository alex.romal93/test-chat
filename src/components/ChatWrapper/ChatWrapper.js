import React from 'react';
import styled from 'styled-components';

const ChatWrapper = props => <div {...props} />;

export default styled(ChatWrapper)`
  display: block;
  width: 500px;
  max-width: 500px;
  margin: 0 auto;
  text-align: center;
  border: 1px solid #eee;
  padding: 20px;
`;
