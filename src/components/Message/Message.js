// @flow

import React from 'react';
import styled from 'styled-components';
import type { MessageType } from 'types/MessageType';
import {
  MESSAGE_TYPE_IMAGE,
  MESSAGE_TYPE_VIDEO
} from 'config/constants';

type Props = {
  message: MessageType
};

const Message = ({ message, ...props }: Props) => {
  const date = new Date(message.date);

  return (
    <div {...props}>
      {message.type === MESSAGE_TYPE_IMAGE && <img src={message.data.trim()} />}
      {message.type === MESSAGE_TYPE_VIDEO && <video src={message.data.trim()} controls />}
      <div className="message">{message.data}</div>
      <div className="name">{message.name}</div>
      <div className="date">{`${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`}</div>
    </div>
  );
};

export default styled(Message)`

`;
