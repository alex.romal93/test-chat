// @flow

import React from 'react';
import cx from 'classnames';
import styled from 'styled-components';

type Props = {
  +hasError: ?boolean,
  +className: ?string
};

const Input = ({ hasError = false, className = '', ...props }: Props) =>
  <input className={cx(className, { hasError })} {...props} />;

export default styled(Input)`
  &.hasError {
    border-color: red;
  }
`;
