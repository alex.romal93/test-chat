// @flow

import React from 'react';
import styled from 'styled-components';
import Input from 'components/Input';
import Button from 'components/Button';

type Props = {
  +send: Function
};

type State = {
  +message: string,
  +hasError: boolean
}

class MessageForm extends React.Component<Props, State> {
  state: State = {
    message: '',
    hasError: false
  };

  constructor(props) {
    super(props);

    this.sendMessage = this.sendMessage.bind(this);
  }

  sendMessage() {
    if (this.state.message.trim() !== '') {
      this.props.send(this.state.message);
      this.setState({ message: '', hasError: false });
    } else {
      this.setState({ hasError: true });
    }
  }

  render() {
    const { login, send, ...props } = this.props;

    return (
      <div {...props}>
        <Input
          type="text"
          hasError={this.state.hasError}
          placeholder="Write your message"
          onChange={e => this.setState({ message: e.target.value, hasError: false })}
          value={this.state.message}
        />
        <Button onClick={this.sendMessage}>Send</Button>
      </div>
    )
  }
}

export default styled(MessageForm)`
  display: block;
  width: 100%;
  margin: 0 auto;
  text-align: center;
  border: 1px solid #eee;
  padding: 20px 0;
  margin: 20px 0;
`;
