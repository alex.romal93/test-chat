// @flow

import React from 'react';
import styled from 'styled-components';

type Props = {
  +name: string,
  +logout: Function
};

const Welcome = ({ name, logout, ...props }: Props) => (
  <div>Welcome {name}! <a href="#login" onClick={e => e.preventDefault() || logout()}>Logout</a></div>
);

export default styled(Welcome)`

`;
