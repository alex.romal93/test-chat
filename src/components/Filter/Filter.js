// @flow

import React from 'react';
import styled from 'styled-components';
import type { MessageType } from 'types/MessageType';
import {
  SORT_DATE_ASC,
  SORT_DATE_DESC,
  FILTER_ALL,
  FILTER_IMAGE,
  FILTER_TEXT,
  FILTER_VIDEO
} from 'config/constants';

type Props = {
  sort: string,
  filter: string,
  setSort: Function,
  setFilter: Function
};

const Filter = ({ sort, filter, setSort, setFilter, ...props }: Props) => (
  <div {...props}>
    <select onChange={e => setSort(e.target.value)} defaultValue={sort}>
      <option value={SORT_DATE_DESC}>Date DESC</option>
      <option value={SORT_DATE_ASC}>Date ASC</option>
    </select>
    <select onChange={e => setFilter(e.target.value)} defaultValue={filter}>
      <option value={FILTER_ALL}>All</option>
      <option value={FILTER_TEXT}>Text</option>
      <option value={FILTER_IMAGE}>Image</option>
      <option value={FILTER_VIDEO}>Video</option>
    </select>
  </div>
);

export default styled(Filter)`

`;
