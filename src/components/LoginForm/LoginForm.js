// @flow

import React from 'react';
import styled from 'styled-components';
import Input from 'components/Input';
import Button from 'components/Button';

type Props = {
  +login: Function
};

type State = {
  +name: string,
  +hasError: boolean
}

class LoginForm extends React.Component<Props, State> {
  state: State = {
    name: '',
    hasError: false
  };

  validateName() {
    const hasError = this.state.name.trim() === '';
    this.setState({ hasError });
    return !hasError;
  }

  render() {
    const { login, ...props } = this.props;

    return (
      <div {...props}>
        <Input
          type="text"
          hasError={this.state.hasError}
          placeholder="Put your name"
          onChange={e => this.setState({ name: e.target.value, hasError: false })}
          defaultValue={this.state.name}
        />
        <Button onClick={() => this.validateName() && this.props.login(this.state.name)}>Login</Button>
      </div>
    )
  }
}

export default styled(LoginForm)`
  display: block;
  width: 500px;
  max-width: 500px;
  margin: 0 auto;
  text-align: center;
  border: 1px solid #eee;
  padding: 20px;
`;
