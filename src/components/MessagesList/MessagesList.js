// @flow

import React from 'react';
import styled from 'styled-components';
import type { MessageType } from 'types/MessageType';
import Message from 'components/Message';
import {
  FILTER_ALL,
  FILTER_IMAGE,
  FILTER_TEXT, FILTER_VIDEO,
  MESSAGE_TYPE_IMAGE,
  MESSAGE_TYPE_TEXT,
  MESSAGE_TYPE_VIDEO,
  SORT_DATE_ASC,
  SORT_DATE_DESC
} from 'config/constants';

type Props = {
  messages: Array<MessageType>,
  sort: string,
  filter: string
};

const MessagesList = ({ messages, sort, filter, ...props }: Props) => {
  const sortedMessages = messages.filter(message => {
    switch (filter) {
      case FILTER_ALL:
        return true;
      case FILTER_TEXT:
        return message.type === MESSAGE_TYPE_TEXT;
      case FILTER_VIDEO:
        return message.type === MESSAGE_TYPE_VIDEO;
      case FILTER_IMAGE:
        return message.type === MESSAGE_TYPE_IMAGE;
      default:
        return false;
    }
  }).sort((a, b) => {
    switch (sort) {
      case SORT_DATE_ASC:
        return a.date < b.date;
      case SORT_DATE_DESC:
        return a.date > b.date;
      default:
        return 1;
    }
  });

  return (
    <div {...props}>
      {sortedMessages.map((message, key) => (
        <Message key={`message-${key}`} message={message}/>
      ))}
    </div>
  );
};

export default styled(MessagesList)`

`;
